import smbus

DISPLAY_128_64 = 1
DISPLAY_128_32 = 2
DISPLAY_96_16 =  3

class SSD1306:
    
    def __init__(self, port, address, type):
        self.port = port
        self.address = address
        self.type = type
        self.bus = None
        self.width = 128
        self.height = 64
        
        if (self.type == DISPLAY_128_64):
            self.width = 128
            self.height = 64        
        
        if (self.type == DISPLAY_128_32):
            self.width = 128
            self.height = 32
            
        if (self.type == DISPLAY_96_16):
            self.width = 96
            self.height = 16

    def open(self):
        self.bus = smbus.SMBus(self.port)
        self.init()
    
    def close(self):
        self.bus = None
        
    def _send(self, data):
        self.bus.write_byte_data(self.address, 0x00, data)
        
    def init(self):
        self._send(0xAE)
        self._send(0xD5)
        self._send(0x80)
        self._send(0xA8)
        
        if (self.type == DISPLAY_128_64):
            self._send(0x3F)
        elif (self.type == DISPLAY_128_32):
            self._send(0x1F)
        else:
            self._send(0x0F)
        
        self._send(0xD3)
        self._send(0x0)
        
        if (self.type == DISPLAY_96_16):
            self._send(0x40 | 0x08)
        else:
            self._send(0x40 | 0x0)
            
        self._send(0x8D)
        
        self._send(0x14)
        
        self._send(0x20)
        self._send(0x0)
        self._send(0x21)
        self._send(0x0)
    
        if (self.type == DISPLAY_128_64):
            self._send(0x7F)
        elif (self.type == DISPLAY_128_32):
            self._send(0x7F)
        else:
            self._send(95)
            
        self._send(0x22)
        self._send(0x0)
        
        if (self.type == DISPLAY_128_64):
            self._send(0x07)
        elif (self.type == DISPLAY_128_32):
            self._send(0x07)
        else:
            self._send(0x01)
            
        self._send(0xA0 | 0x1)
        self._send(0xC8)
        self._send(0xDA)

        if (self.type == DISPLAY_128_64):
            self._send(0x12)
        elif (self.type == DISPLAY_128_32):
            self._send(0x02)
        else:
            self._send(0x02)

        self._send(0x81)
        
        if (self.type == DISPLAY_128_64):
            self._send(0xCF)
        elif (self.type == DISPLAY_128_32):
            self._send(0x8F)
        else:
            self._send(0xAF)
        
        self._send(0xD9)        
        self._send(0xF1)
        
        self._send(0xDB)
        self._send(0xA4)
        self._send(0xA6)
        self._send(0xAF)
        
    def paint(self, picture):
        self._send(0x00 | 0x0)
        self._send(0x10 | 0x0)
        self._send(0x0)
        
        picture = picture.convert("1");
        picture = picture.crop((0, 0, self.width, self.height))
        
        data = [0x00] * (self.width*self.height/8)
        
        pixels = picture.load()
        
        for x in range(0, self.width):            
            for y in range(0,self.height):
                #pixel = picture.getpixel((x,y))
                pixel = pixels[x, y]
                if (pixel == 255):
                    data[(y/8*self.width)+x] |= (1 << y%8)
                else:
                    data[(y/8*self.width)+x] |= (0 << y%8)

        for i in range(0,len(data)/16):
            self.bus.write_i2c_block_data(self.address, 0x40, data[(i*16):(i*16+16)])