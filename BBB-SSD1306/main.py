# -*- coding: utf-8 -*-
from PIL import Image, ImageDraw, ImageFont
from SSD1306 import DISPLAY_128_64
from SSD1306 import SSD1306
import time
import locale
import subprocess

display = SSD1306(1, 0x3C, DISPLAY_128_64)
display.open()

image = Image.new("1",(128,64))

draw = ImageDraw.Draw(image)

bigFont = ImageFont.truetype("arial.ttf", 20)
smallFont = ImageFont.truetype("arial.ttf", 15)

while True:
    p = subprocess.Popen('for i in $(cat /sys/devices/w1_bus_master1/w1_master_slaves); do echo $(echo "scale=3; $(grep \'t=\' /sys/bus/w1/devices/w1_bus_master1/${i}/w1_slave | awk -F \'t=\' \'{print $2}\') / 1000" | bc -l) C; done', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    
    draw.rectangle((0,0,128,64),fill=0)
    
    vert = 0;
    
    for line in p.stdout.readlines():    
        sline = line.rstrip()
        print sline
        
        draw.text((128-draw.textsize(sline, font=bigFont)[0], vert), sline, font=bigFont, fill=255)
        vert = vert + 32        
    
    display.paint(image)